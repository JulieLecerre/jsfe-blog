<?php

namespace App\Models;

use stdClass;


class Model{

    protected $table;

    public function __construct()
    {

    }

    public function user_logged($login,$password)
    {

        $storage = file_get_contents('../storage/user.json');
        $json_data = json_decode($storage, true);
        $user = $json_data['user'];

        if($login==$user['login'] && $password==$user['password']){
            return true;
        }
        return false;
        
    }

    public function dataBlog(): array
    {
        $storage = file_get_contents('../storage/dataBlog.json');
        $json_data = json_decode($storage, true);

        return $json_data['dataBlog'];
    }

}