<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Blogit</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
  <link rel="stylesheet" href="<?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'style.css' ?>" />
</head>

<body>

  <nav class="navbar navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand mb-0 h1" href="./">
        <img src="<?= SCRIPTS . 'images' . DIRECTORY_SEPARATOR . 'mini-git-logo.png' ?>" alt="blogit logo" width="29" height="29" class="d-inline-block align-text-top">
        blogit
      </a>
      <div class="boutonsNav">
        <a href="./articles">
          <button type="button" class="btn btn-secondary">Articles</button>
        </a>
        <a href="./login">
          <button type="button" class="btn btn-secondary">Login</button>
        </a>
      </div>
    </div>
  </nav>

  <div class="container">
    <?= $content ?>
  </div>
</body>

</html>