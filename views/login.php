<form method="POST">
    <div class="mb-3">
        <label for="inputUsername" class="form-label">Username</label>
        <input class="form-control" name="inputUsername" id="inputUsername">
    </div>
    <div class="mb-3">
        <label for="inputPassword" class="form-label">Password</label>
        <input type="password" class="form-control" name="inputPassword" id="inputPassword">
        <?php if ($params['message']) {
        ?>
            <div class="alert alert-danger form-text">
                Invalid password or username
            </div>
        <?php
        } ?>
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
</form>